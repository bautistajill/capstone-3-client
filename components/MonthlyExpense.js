import { useState, useEffect } from 'react'
import { Bar } from 'react-chartjs-2'
import moment from 'moment'



export default function BarChart({rawData}){


	const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
	const [ monthlyExpense, setMonthlyExpense ] = useState([])

	console.log(rawData)

	useEffect(()=>{

		setMonthlyExpense(months.map(month => {

			let expense = 0

			rawData.forEach(element => {

				if(moment(element.categoryDate).format('MMMM') === month){

					expense = expense + parseInt(element.categoryAmount)
				}
			})

			return expense
		}))

	}, [])



	const data = {

		labels: months,
		datasets: [
			{
				label: 'Monthly Expense for the Year 2020',
				backgroundColor: 'indianred',
				borderColor: 'black',
				borderWidth: 1,
				hoverBackgroundColor: 'red',
				hoverBorderColor: 'black',
				data: monthlyExpense
			}

		]
	}

	return(

		<Bar data={data} />

		)
}