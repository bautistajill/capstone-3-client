import { useState, useEffect } from 'react'
import { Bar } from 'react-chartjs-2'
import moment from 'moment'



export default function BarChart({rawData}){


	const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
	const [ monthlyIncome, setMonthlyIncome ] = useState([])



	useEffect(()=>{

		setMonthlyIncome(months.map(month => {

			let income = 0

			rawData.forEach(element => {
				console.log(element)

				if(moment(element.categoryDate).format('MMMM') === month){

					income = income + parseInt(element.categoryAmount)
				}
			})

			return income
		}))

	}, [])



	const data = {

		labels: months,
		datasets: [
			{
				label: 'Monthly Income for the Year 2020',
				backgroundColor: 'indianred',
				borderColor: 'black',
				borderWidth: 1,
				hoverBackgroundColor: 'red',
				hoverBorderColor: 'black',
				data: monthlyIncome
			}

		]
	}


	return(

		<Bar data={data} />

		)




}