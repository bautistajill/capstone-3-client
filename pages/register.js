import {useState, useEffect} from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Router from 'next/router'
import Swal from 'sweetalert2'

export default function Register(){
	const [firstname, setFirstname] = useState('')
	const [lastname, setLastname] = useState('')
	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [isActive, setIsActive] = useState('')

	useEffect(() =>{

		if((firstname !== '' && lastname !== '' && email !== '' && password1 !== '' && password2 !== '') && (password2 === password1)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstname, lastname, email, password1, password2])

	function register(e){

		e.preventDefault()

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/email-exists`, {

			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})

		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data === false){
				fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstname: firstname,
						lastname: lastname,
						email: email,
						password: password1
					})
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					if(data === true){
						Router.push('/')
						
						Swal.fire({
							icon: "success",
							title: "Registered Successfully"
						})
					} else {
						Swal.fire({
							icon: "error",
							title: "Registration Failed"
						})
					}
				})
			} else {
				console.log('Email Already Exists')
				Swal.fire({
					icon: "error",
					title: "Email Already Exists"
				})
			}
		})

	} 

	return(

		<Form onSubmit={(e) => register(e)}>
			<Form.Group controlId="firstname">
				<Form.Label>First Name</Form.Label>
				<Form.Control type="text" placeholder="Enter First Name" value={firstname} onChange={e => setFirstname(e.target.value)} required />
			</Form.Group>
			<Form.Group controlId="lastname">
				<Form.Label>Last Name</Form.Label>
				<Form.Control type="text" placeholder="Enter Last Name" value={lastname} onChange={e => setLastname(e.target.value)} required />
			</Form.Group>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control type="email" placeholder="Enter Email Address" value={email} onChange={e => setEmail(e.target.value)} required />
			</Form.Group>
			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control type="password" placeholder="Enter Password" value={password1} onChange={e => setPassword1(e.target.value)} required />
			</Form.Group>
			<Form.Group controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} required />
			</Form.Group>
			{
				isActive
				? <Button className="button" type="submit" id="submitBtn">Submit</Button>
				: <Button className="button" type="submit" id="submitBtn" disabled>Submit</Button>
			}
		</Form>
		)

}