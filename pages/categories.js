import { useEffect, useState} from 'react'

import { Row } from 'react-bootstrap'
import { Col } from 'react-bootstrap'
import Table from 'react-bootstrap/Table'
import Button from 'react-bootstrap/Button'

export default function categories(){

    
    const [ categories, setCategories ] = useState([])

    useEffect(()=>{

        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {

            headers:{

                Authorization: `Bearer ${localStorage.getItem('token')}`
            }

        })
        .then( res => res.json())
        .then( data => {

            console.log(data)

             if(data){

                setCategories(data.category)

            } else {

                setCategories([])

            }

        })

    }, [])

    const categoryData =  categories.map(category => {

        return(
          
            <tr key={category._id}>
                <td>{category.categoryName}</td>
                <td>{category.categoryType}</td>
            </tr>  

            )
    })

    console.log(categories)
    
    return (
      
        <>
         <h1>Categories</h1>
            <Button href="/addCategory" variant="primary" type="submit" id="addBtn">Add</Button>
                <Row>
                    <Col xs={12} lg={12}>
                        <Table striped bordered hover responsive >
                            <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Type</th> 
                                </tr>
                            </thead>
                            <tbody>
                                {categoryData}
                            </tbody>
                        </Table>   
                    </Col>
                </Row>
        </>
     

        )
}