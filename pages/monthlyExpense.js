import { useState, useEffect } from 'react'
import MonthlyExpense from '../components/MonthlyExpense'
import Head from 'next/head'



export default function monthlyExpense(){

	const [ data, setData ] = useState([])

	useEffect(()=>{

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
			headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
		})
		.then(res => res.json())
		.then(data => {
			setData(data.records)
		})
	}, [])


	const expenseData = data.filter(expense => {
		if(expense.categoryType === 'expense'){
			return expense
		} else {
			return null
		}
	})


	return(
		<>
		<h1>Monthly Expense in PHPs</h1>
		{data.length > 0 ? <MonthlyExpense rawData={expenseData} /> : null}
		</>
		)

}