import {useState, useEffect} from 'react'
import Button from 'react-bootstrap/Button'
import Swal from 'sweetalert2'
import Router from 'next/router'
import Form from 'react-bootstrap/Form'


export default function Records(){

	const [name, setName] = useState('select')
	const [type, setType] = useState('select')
	const [amount, setAmount] = useState('')
	const [description, setDescription] = useState('')
	const [date, setDate] = useState('')
	const [categoryArr, setCategoryArr] = useState([])
	const [balance, setBalance] = useState(0)
	const [isActive, setIsActive] = useState(false)

	/*console.log(amount)
	console.log(balance)*/

	useEffect(()=>{

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`,{

			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				console.log(data)
				setCategoryArr(data.category)
				setBalance(data.balance)
			}
		})
		if(name !== '' && type !== '' && name !== 'select' && type !== 'select' && amount !== 0 && description !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [name, type])

	function editRecords(e){

		e.preventDefault()

		if(type === 'income'){


			fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/editRecords`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					categoryName: name,
					categoryType: type,
					categoryAmount: amount,
					categoryDescription: description,
					categoryDate: date,
					balance: parseInt(amount) + parseInt(balance),
					currentBalance: parseInt(amount) + parseInt(balance)	
				})
			})
			.then(res => {
				return res.json()
			})
			.then(data => {
				if(data === true){
					Swal.fire({
						icon: 'success',
						title: 'Record Updated!'
					})

					setName('')
					setType('')
					setAmount('')
					setDescription('')

					Router.push("/records")

				} else {
					Swal.fire({
						icon: 'error',  
						title: 'Record failed to update!'
					})
				}
			})
		
		} else {

			fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/editRecords`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					categoryName: name,
					categoryType: type,
					categoryAmount: amount,
					categoryDescription: description,
					categoryDate: date,
					balance: parseInt(balance) - parseInt(amount),
					currentBalance: parseInt(balance) - parseInt(amount)	
				})
			})
			.then(res => {
				return res.json()
			})
			.then(data => {
				if(data === true){
					Swal.fire({
						icon: 'success',
						title: 'Record Updated!'
					})

					setName('')
					setType('')
					setAmount('')
					setDescription('')
					setDate('')

					Router.push("/records")

				} else {
					Swal.fire({
						icon: 'error',  
						title: 'Record failed to update!'
					})
				}
			})
		}
	}

	const categoryName = categoryArr.map(category => {
		if(type === 'income'){
			if(category.categoryType == 'income'){
				return(
					<option value={category.categoryName}>{category.categoryName}</option>
				)
			}
		}else{
			if(category.categoryType == 'expense'){
				return(
					<option value={category.categoryName}>{category.categoryName}</option>
				)
			}
		}
		
	})



	return(
		<>
		<h1>New Record</h1>
		<Form onSubmit={(e) => editRecords(e)}>
			<Form.Group controlId="type">
			<Form.Label>Category Type:</Form.Label>
				<Form.Control as='select' value={type} onChange={(e) => setType(e.target.value)} required>
				<option value="select  ">Select</option>
				<option value="income">Income</option>
				<option value="expense">Expense</option>
			</Form.Control>
			</Form.Group>
			<Form.Group controlId="category">
			<Form.Label>Category Name:</Form.Label>
				<Form.Control as='select' placeholder="Category Name" value={name} onChange={(e) => setName(e.target.value)} required >
				<option value={"select"}>Select</option>
				{categoryName}
				</Form.Control>
			</Form.Group>
			<Form.Group controlId="amount">
			<Form.Label>Amount:</Form.Label>
				<Form.Control type="text" placeholder="0" value={amount} onChange={(e) => setAmount(e.target.value)} required />
			</Form.Group>
			<Form.Group controlId="description">
			<Form.Label>Description:</Form.Label>
				<Form.Control type="text" placeholder="Enter Description" value={description} onChange={(e) => setDescription(e.target.value)} required />
			</Form.Group>
			<Form.Group controlId="date">
			<Form.Label>Date:</Form.Label>
				<Form.Control type="date" placeholder="Enter Date" value={date} onChange={(e) => setDate(e.target.value)} required />
			</Form.Group>
			{
				isActive
					?<Button className="button" type="submit" id="submitBtn">Submit</Button>
					:<Button className="button" type="submit" id="submitBtn" disabled>Submit</Button>
			}
			
		</Form>

		</>
		)


}