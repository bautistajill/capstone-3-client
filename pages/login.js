import { useState, useEffect, useContext } from 'react'

import UserContext from '../UserContext'

import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Router from 'next/router'
import Swal from 'sweetalert2'
import { GoogleLogin } from 'react-google-login'

export default function login(){

	const { user, setUser } = useContext(UserContext)

	const [ email, setEmail ] = useState('')
	const [ password, setPassword ] = useState ('')


	function authenticate(e){

		e.preventDefault()

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/login`, {

			method: 'POST',
			headers: {

				'Content-Type' : 'application/json'

			},

			body : JSON.stringify({

				email: email,
				password: password
			})

		}).then(res => {

			return res.json()

		}).then( data => {

			if(data.accessToken){

				localStorage.setItem('token', data.accessToken)

				fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {

					headers: {

						Authorization: `Bearer ${data.accessToken}`

					}
				})
				.then(res => {

					return res.json()

				})
				.then(data => {

					setUser({

						id: data._id,
						email: data.email
						

					})

					Swal.fire({
						icon: 'success',
						title: 'Successful Login'

					})

					Router.push('/')

				})


			} else {
					Swal.fire({
						icon: 'error',
						title: 'Authentication failed.'

					})

			}
		})

		setEmail('')
		setPassword('')
	}

	const authenticateGoogleToken = (response) => {

		console.log(response)

		const payload = {

			method: 'POST',
			headers: {

				'Content-Type': 'application/json'				
			},
			body: JSON.stringify({

				tokenId: response.tokenId,
				googleToken:response.accessToken
			})
		}

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/verify-google-id-token`, payload)
		.then(res => {
			
			return res.json()

		}).then(data => {			

			if( typeof data.access !== 'undefined' ){

				localStorage.setItem('token', data.access)
				fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {

					headers: {

						'Authorization': `Bearer ${data.access}`
					}

				}).then(res => {
					return res.json()

				}).then(data => {
					console.log(data)

					setUser({

						id: data._id,
						email: data.email,
						isAdmin: data.isAdmin
					})

					Swal.fire({
						icon: 'success',
						title: 'Successfully Logged In!'
					})

					Router.push('/')
				})

			} else {

				if( data.error === 'google-auth-error'){

					Swal.fire({

						icon: 'error',
						title: 'Google Authentication Error',
						text: 'Google Authentication Procedure has failed, tyr again or contact your web admin.'
					})

				} else if(data.error === 'login-type-error'){

					Swal.fire({

						icon: 'error',
						title: 'Login Type Error',
						text: 'You may have registered using a different login procedure. Try alternative login procedures'
					})

				} 
			}

		})

	}



	return(

		<Form onSubmit={(e) => authenticate(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control type='email' placeholder='Enter Email' value={email}
					onChange={(e) => setEmail(e.target.value)}/>	
			</Form.Group>
			<Form.Group>
				<Form.Label>Password: </Form.Label>
				<Form.Control type='password' placeholder='Enter Password' value={password}
						onChange={(e) => setPassword(e.target.value)} />
			</Form.Group>
			<Button className="button" type="submit">Submit</Button>
			<GoogleLogin
					clientId='450904727855-5p35gtr68ecb5ekcnsf51e35jkp9bd6s.apps.googleusercontent.com'
					buttontext='Login using Google'
					cookiePolicy={'single_host_origin'}
					onSuccess={authenticateGoogleToken}
					onFailure={authenticateGoogleToken}
					className='w-100 text-center d-flex justify-content-center'
				/>
		</Form>

		)

}