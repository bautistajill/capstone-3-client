import { useState, useEffect, Fragment } from 'react'
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import Navbar from '../components/NavBar'
import {Container} from 'react-bootstrap'
import {UserProvider} from '../UserContext'

function MyApp({ Component, pageProps }) {

	const [ user, setUser ] = useState({
		id : null
	})

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
			headers: {
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data._id){
				setUser({
					id: data._id
				})
			} else {
				setUser ({
					id: null
				})
			}
		})
	}, [user.id])

	const unsetUser = () => {
		localStorage.clear()
		setUser({
			id: null
		})
	}

	return(

		<Fragment>
		<UserProvider value={{user, setUser, unsetUser}}>
		<Navbar />
		<Container>
			<Component {...pageProps} />	
		</Container>
		</UserProvider>
		</Fragment>
		)
}

export default MyApp