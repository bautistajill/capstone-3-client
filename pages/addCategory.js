import {useState} from 'react'
import Button from 'react-bootstrap/Button'
import Swal from 'sweetalert2'
import Router from 'next/router'
import Form from 'react-bootstrap/Form'

export default function Categories(){

	const [name, setName] = useState('')
	const [type, setType] = useState('income')

	function createCategory(e){

		e.preventDefault()

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/addCategory`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				categoryName: name,
				categoryType: type
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data === true){
				Swal.fire({
					icon: 'success',
					title: 'Category successfuly created!'
				})
				Router.push("/categories")
			} else {
				Swal.fire({
					icon: 'error',
					title: 'Category failed to be created!'
				})
				Router.push('/addCategory')
			}
		})

		setName('')
		setType('')
	}


	return(
		<>
		<h1>Categories</h1>
		<Form onSubmit={(e) => createCategory(e)}>
			<Form.Group controlId="category">
			<Form.Label>Category Name:</Form.Label>
			<Form.Control type="text" placeholder="Enter category" value={name} onChange={(e) => setName(e.target.value)} required />
			</Form.Group>
			<Form.Group controlId="type">
			<Form.Label>Category Type:</Form.Label>
			<Form.Control as='select' value={type} onChange={(e) => setType(e.target.value)} required>
				<option value="income">Income</option>
				<option value="expense">Expense</option>
			</Form.Control>
			</Form.Group>
			<Button className="button" type="submit" id="submitBtn">Submit</Button>
		</Form>

		</>
		)


}