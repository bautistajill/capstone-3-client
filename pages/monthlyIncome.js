import { useState, useEffect } from 'react'
import MonthlyIncome from '../components/MonthlyIncome'
import Head from 'next/head'



export default function monthlyIncome(){

	const [ data, setData ] = useState([])

	useEffect(()=>{

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
			headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
		})
		.then(res => res.json())
		.then(data => {
			setData(data.records)
			//console.log(data)
		})
	}, [])


	const incomeData = data.filter(income => {
		if(income.categoryType === 'income'){
			return income
		} else {
			return null
		}
	})

	

	return(
		<>
		<h1>Monthly Income in PHP</h1>
		{data.length > 0 ? <MonthlyIncome rawData={incomeData} /> : null}
		</>
		)

}